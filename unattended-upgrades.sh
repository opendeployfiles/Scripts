dpkg-reconfigure -f noninteractive unattended-upgrades
sudo touch /etc/apt/apt.conf.d/20auto-upgrades
echo "APT::Periodic::Update-Package-Lists "1";" >> /etc/apt/apt.conf.d/20auto-upgrades
echo "APT::Periodic::Unattended-Upgrade "1";" >> /etc/apt/apt.conf.d/20auto-upgrades
/etc/init.d/unattended-upgrades restart
